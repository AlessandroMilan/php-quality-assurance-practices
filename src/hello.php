<?php
/**
 * Hello PHP.
 *
 * PHP version 7.1.1
 *
 * @category Utlis
 * @package  Generic
 * @author   Alessandro Milan <hector.milan@cimat.mx>
 * @license  MIT License
 * @link     http://pear.php.net/package/PackageName
 */

/**
 * <code>
 * // flags: ELLIPSIS
 * echo hello();
 * // expects:
 * // Hello [...].
 * </code>
 */

/**
 * <code>
 * echo hello("MIS");
 * // expects:
 * // Hello MIS.
 * </code>
 */

/**
 * Returns a greeting.
 *
 * @param string $name Person to greet.
 *
 * @return $greeting Greeting.
 */
function hello($name = null) 
{
    $greeting;
    if ($name == null) {
        $greeting = sprintf('Hello %s.', microtime());
    } else {
        $greeting = sprintf('Hello %s.', $name);
    }
    return $greeting;
}
