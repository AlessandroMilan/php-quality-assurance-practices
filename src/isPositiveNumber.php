<?php
/**
 * Is Positive Number PHP.
 *
 * PHP version 7.1.1
 *
 * @category Utlis
 * @package  Generic
 * @author   Alessandro Milan <hector.milan@cimat.mx>
 * @license  MIT License
 * @link     http://pear.php.net/package/PackageName
 */

/**
 * <code>
 * echo isPositiveNumber(1);
 * // expects:
 * // 1
 * </code>
 */

/**
 * Returns 1 if the given number is positive, otherwise returns 0.
 *
 * @param int $number Number to evaluate.
 *
 * @return $result Result of the evaluation.
 */
function isPositiveNumber($number)
{
    $result = $number >= 0;

    return $result;
}
