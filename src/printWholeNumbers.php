<?php
/**
 * Print Whole Numbers
 *
 * PHP version 7.1.1
 *
 * @category Utlis
 * @package  Generic
 * @author   Alessandro Milan <hector.milan@cimat.mx>
 * @license  MIT License
 * @link     http://pear.php.net/package/PackageName
 */

/**
 * <code>
 * printWholeNumbers(3);
 * // expects:
 * //0
 * //1
 * //2
 * </code>
 *
 * <code>
 * printWholeNumbers(3);
 * // expects-file: ./tests/wholeNumbers.txt
 * </code>
 */

/**
 * Given a number N, this function prints ther first N whole numbers.
 *
 * @param int $quantity Ammount of numbers to print.
 *
 * @return void
 */
function printWholeNumbers($quantity)
{
    for ($i=0;$i<$quantity;$i++) {
        echo $i . "\n";
    }
}
